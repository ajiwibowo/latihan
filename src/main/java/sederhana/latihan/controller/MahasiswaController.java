package sederhana.latihan.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import sederhana.latihan.config.RestResponse;
import sederhana.latihan.dto.MahasiswaDto;
import sederhana.latihan.services.MahasiswaSvc;

@RestController
@RequestMapping("/mahasiswa")
public class MahasiswaController {

	@Autowired
	private MahasiswaSvc svc;

	@GetMapping("/")
	public ResponseEntity<RestResponse> getAll() {
		RestResponse response = new RestResponse();

		List<MahasiswaDto> dtos = svc.getAll();

		if (dtos != null) {
			response.setDataObject(dtos);
			response.setMessage("Data ditemukan.");
			response.setStatus(1);
		} else {
			response.setDataObject(null);
			response.setMessage("Data kosong.");
			response.setStatus(0);
		}

		ResponseEntity<RestResponse> responses = new ResponseEntity<RestResponse>(response, HttpStatus.OK);

		return responses;
	}

	@GetMapping("/{idMahasiswa}")
	public ResponseEntity<RestResponse> getOne(@PathVariable("idMahasiswa") String idMahasiswa) {
		MahasiswaDto dto = svc.getOne(Long.parseLong(idMahasiswa));
		RestResponse response = new RestResponse();

		if (dto != null) {
			response.setDataObject(dto);
			response.setMessage("Data ditemukan.");
			response.setStatus(1);
		} else {
			response.setDataObject(null);
			response.setMessage("Data kosong.");
			response.setStatus(0);
		}

		ResponseEntity<RestResponse> responses = new ResponseEntity<RestResponse>(response, HttpStatus.OK);

		return responses;
	}

	@PostMapping("/")
	public ResponseEntity<RestResponse> save(@RequestBody MahasiswaDto dto) {
		RestResponse response = new RestResponse();

		String status = svc.save(dto);

		response.setDataObject(dto);
		response.setMessage(status);
		response.setStatus(1);

		ResponseEntity<RestResponse> responses = new ResponseEntity<RestResponse>(response, HttpStatus.OK);

		return responses;
	}
	
	@PutMapping("/")
	public ResponseEntity<RestResponse> update(@RequestBody MahasiswaDto dto) {
		RestResponse response = new RestResponse();

		String status = svc.update(dto);

		response.setDataObject(dto);
		response.setMessage(status);
		response.setStatus(1);

		ResponseEntity<RestResponse> responses = new ResponseEntity<RestResponse>(response, HttpStatus.OK);

		return responses;
	}
	
	@DeleteMapping("/{idMahasiswa}")
	public ResponseEntity<RestResponse> update(@PathVariable("idMahasiswa")Long idMahasiswa) {
		RestResponse response = new RestResponse();
		
		try {
			svc.delete(idMahasiswa);
			response.setDataObject(null);
			response.setMessage("Berhasil delete.");
			response.setStatus(1);
		}catch(Exception e) {
			response.setDataObject(null);
			response.setMessage("Gagal delete.");
			response.setStatus(1);
		}
		
		ResponseEntity<RestResponse> responses = new ResponseEntity<RestResponse>(response, HttpStatus.OK);

		return responses;
	}
}
