package sederhana.latihan.services;

import java.util.List;

import sederhana.latihan.dto.MahasiswaDto;

public interface MahasiswaSvc {

	public List<MahasiswaDto> getAll();
	public MahasiswaDto getOne(Long idMahasiswa);
	public String save(MahasiswaDto dto);
	public String update(MahasiswaDto dto);
	public String delete(Long idMahasiswa);
	
}
