package sederhana.latihan.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sederhana.latihan.dao.MahasiswaDao;
import sederhana.latihan.dto.MahasiswaDto;
import sederhana.latihan.entity.Mahasiswa;
import sederhana.latihan.services.MahasiswaSvc;

@Service
@Transactional
public class MahasiswaSvcImpl implements MahasiswaSvc {

	@Autowired
	private MahasiswaDao dao;

	@Override
	public List<MahasiswaDto> getAll() {
		List<Mahasiswa> mhs = new ArrayList<Mahasiswa>();
		List<MahasiswaDto> dtos = new ArrayList<MahasiswaDto>();

		try {
			mhs = dao.findAll();

			if (mhs != null) {
				for (Mahasiswa mahasiswa : mhs) {
					MahasiswaDto dto = new MahasiswaDto();
					dto.setIdMahasiswa(mahasiswa.getIdMahasiswa());
					dto.setIdOrtu(mahasiswa.getIdOrtu());
					dto.setNamaSiswa(mahasiswa.getNamaSiswa());

					dtos.add(dto);
				}

				return dtos;
			}

			return null;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}
	}

	@Override
	public MahasiswaDto getOne(Long idMahasiswa) {
		MahasiswaDto dto = new MahasiswaDto();

		try {
			Mahasiswa mhs = dao.getOne(idMahasiswa);

			dto.setIdMahasiswa(mhs.getIdMahasiswa());
			dto.setIdOrtu(mhs.getIdOrtu());
			dto.setNamaSiswa(mhs.getNamaSiswa());

			return dto;
			
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public String save(MahasiswaDto dto) {
		Mahasiswa mhs = new Mahasiswa();
		
		try {
			mhs.setIdMahasiswa(dto.getIdMahasiswa());
			mhs.setIdOrtu(dto.getIdOrtu());
			mhs.setNamaSiswa(dto.getNamaSiswa());
			
			dao.save(mhs);
			return "Berhasil Simpan.";
		} catch (Exception e1) {
			return "Gagal Simpan, terjadi kesalahan";
		}
	}

	@Override
	public String update(MahasiswaDto dto) {
		Mahasiswa mhs = new Mahasiswa();
		
		try {
			mhs = dao.getOne(dto.getIdMahasiswa());
			System.out.println(mhs.getNamaSiswa());
			
			mhs.setIdMahasiswa(dto.getIdMahasiswa());
			mhs.setIdOrtu(dto.getIdOrtu());
			mhs.setNamaSiswa(dto.getNamaSiswa());
			
			try {
				dao.save(mhs);
				return "Berhasil update.";
			} catch (Exception e1) {
				System.out.println(e1.getMessage());
				return "Gagal update, Terjadi kesalahan.";
			}
		} catch (Exception e) {
			return "Gagal update, data tidak ditemukan";
		}

	}

	@Override
	public String delete(Long idMahasiswa) {
		dao.deleteById(idMahasiswa);
		return null;

	}

}
