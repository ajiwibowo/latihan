package sederhana.latihan.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "mahasiswa")
public class Mahasiswa implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id_mahasiswa")
	private Long idMahasiswa;

	@Column(name = "nama_siswa")
	private String namaSiswa;

	@Column(name = "id_ortu")
	private int idOrtu;

	public Long getIdMahasiswa() {
		return idMahasiswa;
	}

	public void setIdMahasiswa(Long idMahasiswa) {
		this.idMahasiswa = idMahasiswa;
	}

	public String getNamaSiswa() {
		return namaSiswa;
	}

	public void setNamaSiswa(String namaSiswa) {
		this.namaSiswa = namaSiswa;
	}

	public int getIdOrtu() {
		return idOrtu;
	}

	public void setIdOrtu(int idOrtu) {
		this.idOrtu = idOrtu;
	}

}
