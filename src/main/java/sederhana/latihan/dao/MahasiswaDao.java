package sederhana.latihan.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import sederhana.latihan.entity.Mahasiswa;

public interface MahasiswaDao extends JpaRepository<Mahasiswa, Long>{

}
