package sederhana.latihan.dto;

public class MahasiswaDto {

	private Long idMahasiswa;
	private String namaSiswa;
	private int idOrtu;

	public Long getIdMahasiswa() {
		return idMahasiswa;
	}

	public void setIdMahasiswa(Long idMahasiswa) {
		this.idMahasiswa = idMahasiswa;
	}

	public String getNamaSiswa() {
		return namaSiswa;
	}

	public void setNamaSiswa(String namaSiswa) {
		this.namaSiswa = namaSiswa;
	}

	public int getIdOrtu() {
		return idOrtu;
	}

	public void setIdOrtu(int idOrtu) {
		this.idOrtu = idOrtu;
	}

}
